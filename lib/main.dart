import 'package:flutter/material.dart';

import 'widget/item_screen1.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const appTitle = 'Drawer Demo';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: MyHomepage(title: appTitle),
    );
  }
}

class MyHomepage extends StatefulWidget {
  MyHomepage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _MyHomepageState createState() => _MyHomepageState(title: title);
}

class _MyHomepageState extends State<MyHomepage> {
  _MyHomepageState({required this.title});
  final String title;
  Widget body = Center(
    child: Text('My page!'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: body,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: const Text('item 1'),
              onTap: () {
                setState(() {
                  body = ItemScreen1();
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              title: const Text('item 2'),
              onTap: () {
                setState(() {
                  body = Center(
                    child: Text('item 2'),
                  );
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              title: const Text('item 3'),
              onTap: () {
                setState(() {
                  body = Center(
                    child: Text('item 3'),
                  );
                  Navigator.pop(context);
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}

